<!--

## Abandoned TODO

Make a mario-like jump. I will start from an unity code posted
by tohperbwell found here
https://forum.unity.com/threads/mario-style-jumping.381906/

Collide big elements slow down strongly the framerate by checking every case
before finding the collide one. I've got some ideas to fix it by checking only
"external" cases or randomize the order and checking in reverse depending on
the direction. So I need to implement a framerate counter.

Maybe keep the layers superposition ? No parallax stuff and why not layer changing enigmas ? Maybe four layers entities/interactive/ground/decor with elements superposition on decor ?

Add a auto walk up and down for "stairs-like" ground.

Actually the player even if declared after the other entities is still behind, maybe its because 'm' is before 'p'...

Make dogerolling not stopped by walls -> DOING IT WITH CLEAN CODE.

-->

## Entities

(differents for each level ?)

←, ↑, →, ↓: Load crushers entities

↔, ↕: Load moving plateform entities

☺, ☻, ☼, ♀, ♂: Load monsters entities (up to 5 differents per level ?)

## Script

The game will be finding your dog (love) throught a Fool's journey.

At the beginning of the game you choose a dog name.

## General gameplay

In the game there is 4 collectibles ♠ ♣ ♥ ♦, swords, wands, cups and coins.

Swords are find up in the air or perious falling zones give additional jumps, cost 3 coins to keep.

Wands give antigravity dodgerolling and can be found in tricky places or near fire/lava, cost 2 coins to keep.

Cups give supplement lifes and can be find in peacefull area or near/in water, cost 1 coin to keep.

Coins allow to keep a certain number of the 3 others for next levels and can be found near/under ground or in rocky areas.

At each level the player sleep in a tent to get to next level.

### Level 0

In the first level (or 0 The Fool) the player will notice that its dog is gone.

After searching (in a very simple and walking right manner, learning to walk in the game) he will find it at the top of a cliff (learn to jump) after jumping on the first rock the dog leaves. At the top of the cliff the player will jump wider and wider (learning to run) pit falls until he finally fall to go to the next level (maybe there is a bonus room in "choas" mode afte the pit falls).

### Level 1

In the second level (or I The Magician) the player meet a magician.

He is a liar and tell the player that he know how to find the dog and even where it is.

After that he show on a table to the player the 4 elements (collectible) in the game : coins ♦, cups ♥, swords ♠, and wands ♣ (wihout explaining wihout explaining what they are for, just saying that the way to find the dog and everything in life).

Then he ask the player to give him 10 elements (to find in the level) after what he will open a door and promise the dog it behind it.

### Level 2

Level in a tomb/vault ?

coins = max life points (find it near rocks or ground) || (buy other elements to merchant)
cups = healing (find it near water or underwater) || (life points ?)
swords = jump size (find it it the air) || (extra jump)
wands = run speed (find it near fire or lava) || (dash ?)

### Level 3

collectibles actually have effect

Level in forest / jungle to access a castle (by secret passages?)

### Level 4

first replayable level

## TODO

Add lava.

Add water.

Add death (by burning, drowning or crushed).

Transfert sprite from one canvas to another.

Including mutliple player very soon. That mean I need to
work on parametric key input soon too !

Explain what is pterodactyl.js and where does it come from ?

Create a sprite list for js loading.

Some box lines are missing in jgs font.

Make a separate script for setup ?

Make a seperate git for audio files.

Make a dialog box (Where is the dog ?).

Make an input box (What is your dog's name ?).

Make collectible items.

Make a presentation page for joan_jump on adelfaure.net.

Make transistion between levels.

Reimplement floodfill.

Add effect layer (with floodfill inside) (or in decor ?)

Try not find a way to improve performances.

make a special devlog about 1 ascii art implementation 2 ascii art as assets (vim as editor) 3 the tool for making music of the game.

Try to do paypercutts advice from https://www.youtube.com/watch?v=VJmaTpAtDf8&list=LL&index=1

Add an foreground decor layer ? (to hide object or secrets behind ?) (or in entities ?)

Try to move camera only when player near screen border.

Make JSON based (with named keys) Element creation.

Make command interface between keys and functions trigger.

Add arrow move keys.

Make an interface menu (with changing controls).

Implement saves.

Clean / simplify code in general.

Make jump / running bar.

Reorder functions in joan_jump.js.

REMEBER THAT THE FOUR SYMBOLS ARE NOT OBVIOUS SO MAKE IT A GAMEPLAY

Fix that display flaw when I guess height / width or screen characters number are not divisible by two.

Make a title screen with automn song.

Make a post about palettes and hex values.

Make the empty 22 levels associated with palettes.

Make a @map sequence ?

Add sound fx

Make proximity detection for collision (x,y,w,h)

Make a beautifull and fun itch page not this ugly one X)

Change init_setup behaviour

Make a between levels merchant section

honno — Today at 7:59 PM
o Id defo recommend putting it on gamejolt.com too, YMMV but I see some devs get some interest there

Missy^~^ — Today at 12:58 PM
:arrow_down: could be used for dodging:xKirax:  @Adel Faure
Would make it far easier:pepeOK:

  [SHIFT]+[→](keep down) = running
 
            →   O
            →  /|)
            →  > |
  [↑](keep more or less down to adjust height) = jump
 
            ↑   O/
            ↑  /|
            ↑  / >
  [SHIFT]+[↑](keep down) = long jump
 
           →↑   O/
           →↑  /|
           →↑  / >
  [↑](down)+[↑](up)+[↑](down) = multiple jumps (consume swords)
 
            ↑   O/ * N
            ↑  /|
            ↑  / >
  [→](down)+[ENTER](down) = dogerolling (work while in airs and consume wands)
 
            →  \ /
            →   |
            →  /O\

arrow keys double jump difficulties and general difficulty
my difficulty in Joan Jump was also with inertia


## DEMO TODO

- dog to restart
- timed level with coins to get
- no extra lifes
- one double jump per jump
- one rolling per jump
- finish bus for getting to city for 100 coins before 3:00 (bus left)
- city ground and collectibles
- magician hotel decor / ground and collectibles
- outro level with credits and playing as the dog
- sound / music when dying
- time when taking the bus
- game doesn't pause when minimize
- taking the bus while being dead issue

## fix DEMO TODO

force tuto end in intro level ? 
time must be not negative.
if time out get a message and a sound (bus and doorkeeper must left).
pointer-events none / unhighlight text.

## Post demo todo 

score screen.

from tytan : twitch
    melvinwm:Drake?
    rosomak360:the main character will not get out of this with any traces of mental health
    rosomak360:Dragonair
    melvinwm::)
    rosomak360:Puff Daddy?
    melvinwm:The CYOA description seems really apt
    rosomak360:nice batlle music
    WormJuiceDev:UE litteraly froze when switching blueprint windows. Drops bag of coins
    Broadcaster
    SubscriberPrime GamingBlackVoidTytan:blackv44Unreal
    blackv44Unreal
    blackv44Unreal
    rosomak360:I don't see anything
    rosomak360:blue screen
    rosomak360:a title screen should pop up there
    rosomak360:something's wrong here
    rosomak360:there's no characters here!
    melvinwm:I have played it, it is running as it should as I remember it
    rosomak360:yes
    melvinwm:Or
    rosomak360:it looked different
    melvinwm:Part is running as it hsould
    rosomak360:let me launch it now
    melvinwm:It actually uses the web-DOM to draw with, or at least it did in a previous version in the jam.
    rosomak360:yes, maybe it's a browser problem
    WormJuiceDev:It froze so hard i needed to restart the pc
    WormJuiceDev:Dayum
    WormJuiceDev:Love the looks of this
    rosomak360:exactly!
    rosomak360:now we're talkin
    WormJuiceDev:If this is procedural sound you did a good job.
    rosomak360:@WormJuiceDev it did happen to me once. I held shift for 8 seconds, and "sticky keys" or whatever you call it popped up
    rosomak360:and I couldn't do anything anymore xd
    melvinwm:To the let
    melvinwm:*left
    rosomak360:fall damage, so watch out
    rosomak360:or "fall one hit ko"
    melvinwm:No, just the 100
    rosomak360:yeah, I don't think the coins stay for the next level
    melvinwm:Remember you can use 'space'
    rosomak360:why "666" though
    melvinwm:Yes
    melvinwm:(in the sky?)
    melvinwm:I wonder whether a health system with fall damage dealing damage, not always instant death, would help indirectly make it clear what heights are safe to fall down from. Though that would make things more complicated. Also, given the game currently, I wonder whether a "stun" would work better than an instant-death.
    melvinwm:Yes
    melvinwm:I didn't know whether to spoil it :(

#2021-07-27

## The end jam : things done between 0.17 and 0.20.3

v0.18

Add a "talking", "freezed" and "can't behave" var for element.

Change acceleration speed.

Limit jump to double jump but disable swords consumption.

Disable wand consumption for antigravitionnal dogerolling but limit it to one per not touching ground period.

Rework level 1 (intro) with an interactive tutorial.

Rework level 2 with only coins to get, timed bus taking event for 100 coins in 3 minutes, interact with dog as a way to restart level.

Work on fullscreen function.


v0.19

Rework global color schemes.

Add a sound effect triggering function.

Add sound effects : jumping, dogerolling, death, getting coins, starting timed event.

Loop music.

Add a check visibility function to know if joan_jump is focused by player and pause game if not.

Change camera system to follow player only when he is near screen limits.

Better "canvas cleaning" after element erasing.

Implement level 3 (city) ground, decor and dialogs.

Fix ponctuation issues.


v0.19.1

Draw the card item to found.

Change framerate handling function and improve global framerate stability and performance.


v0.20

Add the card item to found.

Implement level 4 (ending) ground, decor and dialogs.

Make a different ending if item found.

If item found you can replay the game in a different style.

Add level 3 music : fleur.

Change pterodactyl hiding behaviour.


v0.20.3

Make magician invisible if tuto not done.

Prevent Text highlight.

Fix the failing in timed events.

Add message and sfx for failing timed event.

Fix display issue when width is 1440px.


#2021-07-22

## Some rare footage of flawed gamplay

v0.17

Mostly level flawed design.

Work on country level sprite.

Work on level 2.

#2021-07-21

## You can actually die in the title screen

v0.16

Made title level.

Add automne music.

Small modif on intro level and sprites.

Fix fullscreen issue for chromium based browsers.

Add delayed auto fullscreen function.

Change min-width screen size for x2 font size to 1440px.

Change keys to be arrows based.

Change 'enter' to 'space' for the antigravity dogerolling.

# 2021-07-20

## Hello world            

v0.15

Made city level decor.

Start working on country level decor.

Add la_roue music.

Make introduction level (ground, decor and implementation).

Implement the first dialogs.

Add a freezed status for Elements.

Add a silent statut for boot_log.

# 2021-07-18

## Levels names and palettes

v0.14

Made a 22 levels color palette.

Made a JSON containg levels names and hex values.

Simplify the collide function in pterodactyl (by the use of collideWith).

# 2021-07-17

## Levels and moods

v0.13

Made generic boot_level function to boot any level.

Made a boot player function.

Made a boot log function.

Made element boot_collectible function.

Fix speed feeling by reduce animation speed when upper that 0.3 frame per behaviour update.

Made dummy_level_0.

Made dummy_level_1.

Prepare technicals (end entity for now).

Made a flag sprite.

Fix moving flaw that allow to not die when constantly press jumping key when falling.

Add a set_mood function to change colors and music for each level.

Start to work on color palettes.

# 2021-07-14

## Swords allow double jumps

v0.12

Implementing death by falling.

Made boot, shutdown and reboot function (use in death by falling).

Add a warning note for arch user using i3 with the 89+ version of firefox.

Fall sequence used when player will by hurt by falling (20+ chars fall).

Add all 4 collectible in the game to test added lifes.

Made a stop jump function.

Swords can now be use to make double jump.

Add damage animation (crush).

Add game over animation.

# 2021-07-12

## You can actually collect wands to make antigravity dogerollings !

v0.11

Fix dashing flaw that consume multiple wands when active the command.

Try a dirty way to choose fps, currently 40 seems good to feeling.

Add swords and cups counter.

Wands can be collected.

# 2021-07-11

## Antigravity dogerolling

v0.10

Add some screenshots to itch page.

Declare player entity after the others.

Draw collectible and include it in jgs font.

Remove "interactive" named layer (entities and ground are allready interactive).

Comment the "keep down" function for the time being.

Start working on including collectible in the game with a @collectible sequence in ground sprites)

Add a getCharPos function for graphics in previous to get collectible emplacement from @collectible sequence and place it on the game as entities elements.

Add some sort of dash consuming wands thats will be a collectible.
0
Thanks to @DarioDaf and @Rosomak360 from the SoloDevelopment discord I can call the "dash" "Antigravity dogerolling".

# 2021-07-10

## Stationary magician

v0.9

Include jgs font specimen link.

Continue working on level 0.

Start working on level 1.

Change instants counter to FPS counter.

Change jump behaviour still some flaw but I am finally proud of it.

Draw and include magician sprite.

Find ffmpeg ogg compression to reduce mp3 files to 1-1.5mo

# 2021-07-09

## Stoical dog

v0.8

Made draft ground and decor for level 0.

Change transparency char from █ to ▒.

Change "jump curve/behaviour" a bit.

Remove a jump flaw that make player climb very high when jumping against a wall.

# 2021-07-08

## Wouf

v0.7

Element can't accelerate while jumping.

Change jumping time modifications conditions.

Made a empty level "chunk" template (ground), based on 1920x1080 resolution/vim lines, 9x18 font ratio w209 h55.

Remove mp3 file from all revisions feed.

Draw dog sprite.

# 2021-07-04

## A beginning of level design

v0.6

Start to drawing the first level.

Change layers/canvas names to 'entitites', 'interactive', 'ground', 'decor' and 'infos' (in order of superposition).

Add a hide function to be able to cancel glyphs superposition (between player element and decor canvas for exemple).

Begin to implement music in the game.

Start working on color schemes.

Change jump inertia to be smoother.

# 2021-07-03

## 99% jump perfect

v0.5

Crush animation and triggering when obstructed.

Collide top when jumping stop the jump.

Display drawing instants top left.

Canvas are now responsive.

# 2021-07-02

## Levitation is over

v0.4

Add a log layer for adding text infos on qwerty/azerty mode or print technical
info.

Dirty fix a "levitation issue" in certains case pressing space did that the
jump will never totally end making the player levitate one case above ground.

Add a jump capacity variable.

I'll updated the itch page a bit, adding a logo, a better cover image, styling
the page a bit.

Rename Sprite object to Graphic (because sprites are not "pterodactyl
graphics").

# 2021-07-01

## 99% collision perfect

v0.3

Add qwerty/azerty switch mode by pressing 'm'.

Change logbook.txt to devlog.txt and making a day to day sort of "memo" of what
is done instead of todo list

Add license, readme, move pterodactyl inside joan_jump and publish it on
itch.io

I fully reworked the canvas/sprites printing and collision system based on
sprites coordinates not on where glyphs are printed it seem to work ok but a
need a "when stuck move randomly to the next empty spot" in some cases
