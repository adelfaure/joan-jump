#!/usr/bin/env node

//  Joan Jump is a ASCII art plateform game.
//  Copyright (C) 2021 adelfaure
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.

// PROTOTYPES

Object.defineProperty(Array.prototype, 'shuffle', {
  value: function() {
    for (let i = this.length - 1; i > 0; i--) {
      const j = Math.floor(Math.random() * (i + 1));
      [this[i], this[j]] = [this[j], this[i]];
    }
    return this;
  }
});

// GAME

var no_sfx = false;

// PRESETS
function sfx(name){
  if (no_sfx) return;
  let sound = document.createElement("audio");
  if(name == "death" || name == "start" || name == "item"){
    no_sfx = true;
    document.getElementById('music').volume = 0;
    setTimeout(function(){
      no_sfx = false;
      document.getElementById('music').volume = 1;
    },2000);
  }
  sound.src = 'sound/'+name+'.ogg';
  document.body.appendChild(sound);
  sound.play();
  sound.onended = function() {
    document.body.removeChild(this);
  }
}

function set_mood(bg_color, ground_color, entities_color, decor_color, music_source){
  document.getElementById('wrapper').style.backgroundColor = bg_color;
  document.getElementById('ground').style.color = ground_color;
  document.getElementById('entities').style.color = entities_color;
  document.getElementById('infos').style.color = entities_color;
  document.getElementById('decor').style.color = decor_color;
  if (document.getElementById('music').src.indexOf('sound/'+music_source+'.ogg') == -1) {
    document.getElementById('music').pause();
    document.getElementById('music').src = 'sound/'+music_source+'.ogg';
    document.getElementById('music').play();
  }
};

function boot_player(x, y) {
  player = new Element(
    new Graphic('player', entities, win ? 'dog' : 'player', 'stand', x, y),
    1/3,
    function (element){
      let xVelocity = element.xVelocity * element.dirX;
      let yVelocity = element.yVelocity * element.dirY;
      control(element, 
        keys.arrowright && !keys.arrowleft && xVelocity > -1.1 && xVelocity < 1.1 ? 1:
        keys.arrowleft && !keys.arrowright && xVelocity > -1.1 && xVelocity < 1.1 ? -1:
        xVelocity > 1  ?  1: 
        xVelocity < -1 ? -1: 0, 
        /*keys.arrowup ? -1/3: */1
      );
      //keep_down(element);
      if (element.collide(element.speed * xVelocity, 0) || element.freezed) {
      // stop
        if (element.talking) {
          element.dirX = 0;
          element.dirY = 0;
        }
        element.xVelocity = 1;
        element.yVelocity = 1;
      } else if (element.dashing) {
        yVelocity = 0;
        //xVelocity *= 1.25;
      } else if (!keys.shift 
       || !keys.arrowleft && !keys.arrowright 
       || keys.arrowleft && element.dirX > 0 
       || keys.arrowright && element.dirX < 0) {
        // deccelerate
        accelerate(element, 0, element.xVelocity > 1 ? 0.9: 1);
      } else {
      // accelerate
        accelerate(element, 0, 
          xVelocity > 0 && keys.arrowright && !keys.arrowleft && keys.shift 
          || xVelocity < 0 && keys.arrowleft && !keys.arrowright && keys.shift ? 1+1/20: 1
        );
      }

      // init_double_jump
      if (element.collide(0,1) && !element.dashing && !element.jumping) {
        element.swords = 1;
        element.wands = 1;
      }
      // jump
      if (keys.arrowup 
      && element.collide(0, 1) 
      && !element.collide(0, -1) 
      && element.jumping == false) {
        keys_up.arrowup = false;
        element.jumping = true;
        sfx("jump");
      }
      // double_jump
      if (keys.arrowup 
      && element.jumping == true 
      && keys_up.arrowup 
      && element.swords
      || keys.arrowup 
      && element.jumping == false
      && !element.collide(0, 1)
      && keys_up.arrowup 
      && element.swords ) {
        keys_up.arrowup = false;
        stop_jump(element);
        element.jumping = true;
        element.swords = 0;
        element.double_jump = true;
        sfx("jump");
      }
      if (keys[' '] 
      && element.dashing == false 
      && element.wands 
      && element.dirX != 0) {
        console.log(element.dashing);
        sfx("doge");
        element.dashing = true;
        element.wands = 0;
        if (keys.arrowright && !keys.arrowleft) {
          if (xVelocity < 0) xVelocity *= -1;
          element.dirX = 1;
          element.graphic.changeSequence('dash_right');
        } 
        if (keys.arrowleft && !keys.arrowright ) {
          if (xVelocity > 0) xVelocity *= -1;
          element.dirX = -1;
          element.graphic.changeSequence('dash_left');
        }
      }
      if (element.dashing && element.graphic.sequenceIndex > element.graphic.sequences[element.graphic.sequence].length-1
      ) {
        element.dashing = false;
      }
      moving(element, 
          element.speed * xVelocity, 
                      1 * yVelocity,    
        [ 'jump_left',  'jump', 'jump_right', 
        /**/(element.dashing ? 'dash_left' : keys.s ? 'crouch_left' :/**/ 'stand_left'/**/)/**/, 
        /**/(keys.s ? 'crouch'      :/**/ 'stand'/**/)/**/,
        /**/(element.dashing ? 'dash_right' : keys.s ? 'crouch_right' :/**/ 'stand_right'/**/)/**/,
          'jump_left','jump','jump_right' ], 
        element.speed/1.5*(element.xVelocity > 0 ? element.xVelocity: 0));
      if(!element.freezed) jump(element);
      obstruct(element);
      element.graphic.hide(decor);
    }, 8.5/*8*/
  );
  console.log('player booted on ',x,y);
};

function boot_log(silent){
    if (!silent) {
      new Element(
        new Graphic('log', infos, 'log', 'initial', w-20, 0),
        0,
      );
    }
    new Element(
      new Graphic('log', infos, 'log', 'framerate', 2, 0),
      0,
    );
    //new Element(
    //  new Graphic('log', infos, 'log', 'swords', 2, 1),
    //  0,
    //);
    //new Element(
    //  new Graphic('log', infos, 'log', 'wands', 2, 2),
    //  0,
    //);
    //new Element(
    //  new Graphic('log', infos, 'log', 'cups', 2, 3),
    //  0,
    //);
    new Element(
      new Graphic('log', infos, 'log', 'time', 2, 1),
      0,
    );
    new Element(
      new Graphic('log', infos, 'log', 'coins', 2, 2),
      0,
    );

};

// @LEVELS
const levels = [
  function() {
    // @LEVEL0
   set_mood("#00C", "#0FF", "#FFF", "#F8C", 'automne');

    /* ## init ground elements */

    new Element(new Graphic('ground', ground, 'title', 'ground', 0, 0), 0, function(element){
      element.graphic.hide(decor);
      if (keys['enter']) {
        shutdown();
        if (win){
          boot_level(current_level+2);
        } else {
          boot_level(current_level+1);
        }
        decor.hide = {};
        ground.hide = {};
      }
    }, 0, 0, true);
    
    /* ## init decor */

    new Element(new Graphic('decor', decor, 'title', 'decor', 0, 0), 0);
    //new Element(new Graphic('decor', decor, 'intro', 'decorB', 0, -56), 0);
    //new Element(new Graphic('decor', decor, 'intro', 'decorC', 0, -56*2), 0);
    boot_player(50, 19);
    player.wands = Infinity;
    player.swords = Infinity;
  },
function() {
  // @LEVEL1
  set_mood("#101", "#F26", "#88F", "#DDB", 'la_roue');

  //boot_log(true);
  
  //new Element(new Graphic('magician', entities, 'magician', 'stand', 1087, 234), 0);
  
  /* ## init ground elements */

  new Element(new Graphic('ground', ground, 'intro', 'ground', 0, 0), 0, false, 0, 0, true);
  
  /* ## init decor */

  new Element(new Graphic('decor', decor, 'intro', 'decor', 0, 0), 0);
  new Element(new Graphic('decor', decor, 'intro', 'decorB', 0, -56), 0);
  new Element(new Graphic('decor', decor, 'intro', 'decorC', 0, -56*2), 0);
  
  /* ## init entities */
  dialog = 1; 
  tuto = 0;
  var magician = new Element(new Graphic('magician', entities, 'magician', 'invisible', 165, 26), 0, function(element) 
    {
      if (player.collide(0,1) && dialog == 1){
        dialog = 1.5;
        new Element(new Graphic('dialog', infos, 'intro', 'dialog1', 0, 0), 0, function(element){
          element.graphic.hide(decor);
          element.graphic.hide(ground);
          if (keys['enter']) {
            dialog = 2;
            keys['enter'] = false;
            keys_up['enter'] = false;
            erase(element);
          }
        });
      }
      if (dialog == 2){
        dialog = 2.5;
        new Element(new Graphic('dialog', infos, 'intro', 'dialog2', 0, 0), 0, function(element){
          element.graphic.hide(decor);
          element.graphic.hide(ground);
          if (keys['enter'] && keys_up['enter']) {
            dialog = 3;
            keys['enter'] = false;
            keys_up['enter'] = false;
            erase(element);
          }
        });
      }
      if (dialog == 3){
        dialog = 3.5;
        new Element(new Graphic('dialog', infos, 'intro', 'dialog3', w-35, 0), 0, function(element){
          element.graphic.hide(decor);
          element.graphic.hide(ground);
          if (keys['enter'] && keys_up['enter']) {
            dialog = 3.75;
            keys['enter'] = false;
            keys_up['enter'] = false;
            erase(element);
          }
        });
      }
      if (dialog == 3.75){
        dialog = 3.99;
        new Element(new Graphic('dialog', infos, 'intro', 'dialog1', 0, 0), 0, function(element){
          element.graphic.hide(decor);
          element.graphic.hide(ground);
          if (keys['enter']) {
            dialog = 4;
            keys['enter'] = false;
            keys_up['enter'] = false;
            erase(element);
          }
        });
      }
      if (dialog == 4){
        dialog = 4.5;
        new Element(new Graphic('dialog', infos, 'intro', 'dialog4', 0, 0), 0, function(element){
          element.graphic.hide(decor);
          element.graphic.hide(ground);
          if (keys['enter'] && keys_up['enter']) {
            dialog = 5;
            keys['enter'] = false;
            keys_up['enter'] = false;
            erase(element);
          }
        });
      }
      if (dialog == 5){
        dialog = 5.5;
        new Element(new Graphic('dialog', infos, 'intro', 'dialog5', 0, 0), 0, function(element){
          element.graphic.hide(decor);
          element.graphic.hide(ground);
          if (keys['enter'] && keys_up['enter']) {
            dialog = 6;
            keys['enter'] = false;
            keys_up['enter'] = false;
            erase(element);
          }
        });
      }
      if (dialog == 6){
        dialog = 6.5;
        new Element(new Graphic('dialog', infos, 'intro', 'dialog6', w-35, 0), 0, function(element){
          element.graphic.hide(decor);
          element.graphic.hide(ground);
          if (keys['enter'] && keys_up['enter']) {
            dialog = 7;
            keys['enter'] = false;
            keys_up['enter'] = false;
            erase(element);
          }
        });
      }
      if (dialog == 7){
        dialog = 7.5;
        new Element(new Graphic('dialog', infos, 'intro', 'dialog7', w-35, 0), 0, function(element){
          element.graphic.hide(decor);
          element.graphic.hide(ground);
          if (keys['enter'] && keys_up['enter']) {
            dialog = 8;
            keys['enter'] = false;
            keys_up['enter'] = false;
            erase(element);
          }
        });
      }
      if (dialog == 8){
        dialog = 8.5;
        new Element(new Graphic('dialog', infos, 'intro', 'dialog8', w-35, 0), 0, function(element){
          element.graphic.hide(decor);
          element.graphic.hide(ground);
          if (keys['enter'] && keys_up['enter']) {
            dialog = 9;
            keys['enter'] = false;
            keys_up['enter'] = false;
            erase(element);
          }
        });
      }
      if (dialog == 9){
        dialog = 9.5;
        new Element(new Graphic('dialog', infos, 'intro', 'dialog9', 0, 0), 0, function(element){
          element.graphic.hide(decor);
          element.graphic.hide(ground);
          if (keys['enter'] && keys_up['enter']) {
            dialog = 10;
            keys['enter'] = false;
            keys_up['enter'] = false;
            erase(element);
          }
        });
      }
      if (dialog == 10){
        dialog = 10.5;
        new Element(new Graphic('dialog', infos, 'intro', 'dialog10', w-35, 0), 0, function(element){
          element.graphic.hide(decor);
          element.graphic.hide(ground);
          if (keys['enter'] && keys_up['enter']) {
            dialog = 11;
            keys['enter'] = false;
            keys_up['enter'] = false;
            erase(element);
          }
        });
      }
      if (dialog == 11){
        dialog = 11.5;
        new Element(new Graphic('dialog', infos, 'intro', 'dialog11', 0, 0), 0, function(element){
          element.graphic.hide(decor);
          element.graphic.hide(ground);
          if (keys['enter'] && keys_up['enter']) {
            dialog = 12;
            keys['enter'] = false;
            keys_up['enter'] = false;
            erase(element);
          }
        });
      }
      if (dialog == 12){
        dialog = 12.5;
        new Element(new Graphic('dialog', infos, 'intro', 'dialog12', w-35, 0), 0, function(element){
          element.graphic.hide(decor);
          element.graphic.hide(ground);
          if (keys['enter'] && keys_up['enter']) {
            dialog = 13;
            keys['enter'] = false;
            keys_up['enter'] = false;
            erase(element);
          }
        });
      }
      if (dialog == 13){
        dialog = 13.5;
        new Element(new Graphic('dialog', infos, 'intro', 'dialog13', w-35, 0), 0, function(element){
          element.graphic.hide(decor);
          element.graphic.hide(ground);
          if (keys['enter'] && keys_up['enter']) {
            dialog = 14;
            keys['enter'] = false;
            keys_up['enter'] = false;
            erase(element);
          }
        });
      }
      if (dialog == 14){
        dialog = 14.5;
        new Element(new Graphic('dialog', infos, 'intro', 'dialog14', w-35, 0), 0, function(element){
          element.graphic.hide(decor);
          element.graphic.hide(ground);
          if (keys['enter'] && keys_up['enter']) {
            keys['enter'] = false;
            keys_up['enter'] = false;
            decor.hide = {};
            ground.hide = {};
            dialog = false;
            erase(element);
          }
        });
      }
      if (player.freezed && !dialog) {
        player.freezed = false;
        tuto = 1;
      }
      if (tuto == 1){
        new Element(new Graphic('tuto', infos, 'intro', 'tuto1', Math.round(w/2-9), 
        0), 0, function(element){
          element.graphic.hide(decor);
          element.graphic.hide(ground);
          element.graphic.hide(entities);
          tuto = 1.5;
          if (keys['arrowleft'] || keys['arrowright']) {
            decor.hide = {};
            ground.hide = {};
            entities.hide = {};
            keys['enter'] = false;
            keys_up['enter'] = false;
            erase(element);
            tuto = 2;
          }
        });
      }
      if (tuto == 2){
        new Element(new Graphic('tuto', infos, 'intro', 'tuto2', Math.round(w/2-14), 
        0), 0, function(element){
          element.graphic.hide(decor);
          element.graphic.hide(ground);
          element.graphic.hide(entities);
          tuto = 2.5;
          if (keys['arrowleft'] && keys['shift'] || keys['arrowright'] && keys['shift']) {
            decor.hide = {};
            ground.hide = {};
            entities.hide = {};
            keys['enter'] = false;
            keys_up['enter'] = false;
            erase(element);
            tuto = 3;
          }
        });
      }
      if (tuto == 3){
        new Element(new Graphic('tuto', infos, 'intro', 'tuto3', Math.round(w/2-9), 
        0), 0, function(element){
          element.graphic.hide(decor);
          element.graphic.hide(ground);
          element.graphic.hide(entities);
          tuto = 3.5;
          if (keys['arrowup']) {
            decor.hide = {};
            ground.hide = {};
            entities.hide = {};
            keys['enter'] = false;
            keys_up['enter'] = false;
            erase(element);
            tuto = 4;
            // decor.hide = {};
            // ground.hide = {};
            // entities.hide = {};
            // boot_log(true);
            // bg.boot_collectibles();
            // time = 180000;
            // bus_init = true;
            // player.freezed = false;
            // player.talking = false;
            // dialog = 1;
          }
        });
      }
      if (tuto == 4){
        new Element(new Graphic('tuto', infos, 'intro', 'tuto4', Math.round(w/2-14), 
        0), 0, function(element){
          element.graphic.hide(decor);
          element.graphic.hide(ground);
          element.graphic.hide(entities);
          tuto = 4.5;
          
          if (player.double_jump) {
            decor.hide = {};
            ground.hide = {};
            entities.hide = {};
            keys['enter'] = false;
            keys_up['enter'] = false;
            erase(element);
            tuto = 5;
            // decor.hide = {};
            // ground.hide = {};
            // entities.hide = {};
            // boot_log(true);
            // bg.boot_collectibles();
            // time = 180000;
            // bus_init = true;
            // player.freezed = false;
            // player.talking = false;
            // dialog = 1;
          }
        });
      }
      if (tuto == 5){
        new Element(new Graphic('tuto', infos, 'intro', 'tuto5', Math.round(w/2-20), 
        0), 0, function(element){
          element.graphic.hide(decor);
          element.graphic.hide(ground);
          element.graphic.hide(entities);
          tuto = 5.5;
          if (keys['arrowleft'] && keys[' '] || keys['arrowright'] && keys[' ']) {
            decor.hide = {};
            ground.hide = {};
            entities.hide = {};
            keys['enter'] = false;
            keys_up['enter'] = false;
            magician.graphic.changeSequence('stand');
            tuto = 6;
            erase(element);
            // decor.hide = {};
            // ground.hide = {};
            // entities.hide = {};
            // boot_log(true);
            // bg.boot_collectibles();
            // time = 180000;
            // bus_init = true;
            // player.freezed = false;
            // player.talking = false;
            // dialog = 1;
          }
        });
      }
      if (element.graphic.collideWith(player.graphic,0,0) && tuto == 6) {
        level = current_level;
        level +=1;
        if (current_level == 1){
           level = 2;
        }else if (current_level == 2) level = 0;
          shutdown();
          boot_level(level);
      }
    });
  
  /* ### init player */
 
  boot_player(40, -56*3);
  player.cups = 1;
  player.freezed = true;
}, 
  function() {
    // @LEVEL2
//["#032", "#FCA", "#A34", "#593"]
    var bus_init = false;
    set_mood("#205", "#FC3", "#EFF", "#2C2", 'cascade');
    /* ## init ground elements */
    var bg = new Element(new Graphic('decor', decor, 'country', 'decor', 0, 0), 0, false, 0, 0/*, true*/);
    /* ## init decor */
    new Element(new Graphic('ground', ground, 'country', 'ground', 0, 0), 0,function(element){
    });
    /* ## init entities */
    dialog = 1; 
    new Element(new Graphic('dog_entity', entities, 'dog', 'stand_left', 137, 47), 0,function(element){
      element.graphic.hide(decor);
      if ( element.graphic.collideWith(player.graphic,-3,0)
        || element.graphic.collideWith(player.graphic,3,0)) {
        if (player.graphic.x > element.graphic.x){
          element.graphic.changeSequence('interact_right');
        } else {
          element.graphic.changeSequence('interact_left');
        }
        if (keys['enter'] && dialog == 1) {
          dialog = 1.5;
          player.freezed = true;
          player.talking = true;
          keys['enter'] = false;
          keys_up['enter'] = true;
          new Element(new Graphic('dialog', infos, 'country', 'dialog2', w-35, 0), 0, function(element){
            element.graphic.hide(decor);
            element.graphic.hide(ground);
            element.graphic.hide(entities);
            if (keys['e']) {
              decor.hide = {};
              ground.hide = {};
              entities.hide = {};
              reboot_level(current_level);
            }
            if (keys['enter'] && keys_up['enter']) {
              keys['enter'] = false;
              keys_up['enter'] = false;
              decor.hide = {};
              ground.hide = {};
              entities.hide = {};
              dialog = 1;
              console.log("coucou",dialog);
              erase(element);
            }
          });
          // new Element(new Graphic('dialog', infos, 'country', 'dialog1', 0, 0), 0, function(element){
          //   element.graphic.hide(decor);
          //   element.graphic.hide(ground);
          //   element.graphic.hide(entities);
          //   console.log(keys['enter']);
          //   if (keys['enter']) {
          //     dialog = 2;
          //     keys['enter'] = false;
          //     keys_up['enter'] = false;
          //     decor.hide = {};
          //     ground.hide = {};
          //     entities.hide = {};
          //     erase(element);
          //   }
          // });
        }
      } else {
        if (player.graphic.x > element.graphic.x){
          element.graphic.changeSequence('stand_right');
        } else {
          element.graphic.changeSequence('stand_left');
        }
      }

      //if (dialog == 2){
      //  dialog = 2.5;
      //  new Element(new Graphic('dialog', infos, 'country', 'dialog2', w-35, 0), 0, function(element){
      //    element.graphic.hide(decor);
      //    element.graphic.hide(ground);
      //    element.graphic.hide(entities);
      //    if (keys['e']) {
      //      decor.hide = {};
      //      ground.hide = {};
      //      entities.hide = {};
      //      reboot_level(current_level);
      //    }
      //    if (keys['enter'] && keys_up['enter']) {
      //      keys['enter'] = false;
      //      keys_up['enter'] = false;
      //      decor.hide = {};
      //      ground.hide = {};
      //      entities.hide = {};
      //      dialog = 1;
      //      console.log("coucou",dialog);
      //      erase(element);
      //    }
      //  });
      //}
      if (player.freezed && dialog == 1) {
        player.freezed = false;
        player.talking = false;
      }
    });
    bus_left_info = false;
    new Element(new Graphic('entity', entities, 'country', 'bus', 174, 43), 0,function(element){
      element.graphic.hide(decor);
      element.graphic.hide(ground);
      if (!Math.round(time/1000) 
      && !bus_left_info) {
        bus_left_info = true;
        player.freezed = true;
        player.talking = true;
        sfx("start");
        new Element(new Graphic('dialog', infos, 'country', 'timeout', Math.round(w/2-10), 
        Math.round(h/2-3)), 0, function(element){
          element.graphic.hide(decor);
          element.graphic.hide(ground);
          element.graphic.hide(entities);
          if (keys['enter'] && keys_up['enter']) {
            keys['enter'] = false;
            keys_up['enter'] = false;
            decor.hide = {};
            ground.hide = {};
            entities.hide = {};
            time = 0;
            player.freezed = false;
            player.talking = false;
            erase(element);
          }
        });
      }
      if (!Math.round(time/1000)) {
        element.graphic.x ++;
      }
      if ( element.graphic.collideWith(player.graphic,-3,0)
        || element.graphic.collideWith(player.graphic,3,0)) {
        if ( bus_init ) {
          element.graphic.changeSequence('bus_price');
          if (keys['enter'] && player.coins >= 100 && time && !player.cant_behave) {
            keys['enter'] = false;
            keys_up['enter'] = false;
            decor.hide = {};
            ground.hide = {};
            entities.hide = {};
            erase(player);
            time = 0;
            bus_left_info = true;
            player.coins = 0;
            setTimeout(function(){
              shutdown(current_level);
              element.graphic.hide(decor);
              element.graphic.hide(ground);
              element.graphic.hide(entities);
              boot_level(current_level + 1)
            },2000);
          }
        } else if (dialog == 1) {
          element.graphic.changeSequence('bus_init');
          if (keys['enter']) {
            player.freezed = true;
            player.talking = true;
            keys['enter'] = false;
            keys_up['enter'] = true;
            dialog = 3;
            new Element(new Graphic('dialog', infos, 'country', 'dialog3', Math.round(w/2-17), 
            Math.round(h/2-4)), 0, function(element){
              element.graphic.hide(decor);
              element.graphic.hide(ground);
              element.graphic.hide(entities);
              if (keys['enter'] && keys_up['enter']) {
                keys['enter'] = false;
                keys_up['enter'] = false;
                decor.hide = {};
                ground.hide = {};
                entities.hide = {};
                boot_log(true);
                bg.boot_collectibles();
                sfx("start");
                time = 180000;
                bus_init = true;
                player.freezed = false;
                player.talking = false;
                dialog = 1;
                erase(element);
              }
            });
          }
        }
      } else {
        element.graphic.changeSequence('bus');
      }
    });
    /* ## init player */
    boot_player(95, 34);
  },
  function() {
    // @LEVEL3
    set_mood('#000', '#688', '#CDD', '#448', 'fleur');
    var bg = new Element(new Graphic('decor', decor, 'city', 'decor', 209*0, 55*0), 0, false, 0, 0);
    new Element(new Graphic('ground', ground, 'city', 'ground', 209*0, 55*0), 0);

    /* init entities */
    if (!card){
      new Element(
        new Graphic('entity', entities, 'city', 'item', 
          107, 
          2, 0), 0,
          function(element) {
            if (element.graphic.collideWith(player.graphic,0,0)) {
              sfx("item");
              new Element(new Graphic('dialog', infos, 'city', 'fool_card', Math.round(w/2-7), 
              Math.round(h/2-6)), 0, function(element){
                element.graphic.hide(decor);
                element.graphic.hide(ground);
                element.graphic.hide(entities);
                pause = true;
                setTimeout(function(){
                  decor.hide = {};
                  ground.hide = {};
                  entities.hide = {};
                  erase(element);
                  pause = false;
                },4000);
              });
              erase(element);
              card = true;
            }
      });
    }
    new Element(new Graphic('dog_entity', entities, 'dog', 'stand_left', 100, 47), 0,function(element){
      element.graphic.hide(decor);
      if ( element.graphic.collideWith(player.graphic,-3,0)
        || element.graphic.collideWith(player.graphic,3,0)) {
        if (player.graphic.x > element.graphic.x){
          element.graphic.changeSequence('interact_right');
        } else {
          element.graphic.changeSequence('interact_left');
        }
        if (keys['enter'] && dialog == 1) {
          dialog = 1.5;
          player.freezed = true;
          player.talking = true;
          keys['enter'] = false;
          keys_up['enter'] = true;
          new Element(new Graphic('dialog', infos, 'country', 'dialog2', w-35, 0), 0, function(element){
            element.graphic.hide(decor);
            element.graphic.hide(ground);
            element.graphic.hide(entities);
            if (keys['e']) {
              decor.hide = {};
              ground.hide = {};
              entities.hide = {};
              reboot_level(current_level);
            }
            if (keys['enter'] && keys_up['enter']) {
              keys['enter'] = false;
              keys_up['enter'] = false;
              decor.hide = {};
              ground.hide = {};
              entities.hide = {};
              dialog = 1;
              console.log("coucou",dialog);
              erase(element);
            }
          });
        }
      } else {
        if (player.graphic.x > element.graphic.x){
          element.graphic.changeSequence('stand_right');
        } else {
          element.graphic.changeSequence('stand_left');
        }
      }
      if (player.freezed && dialog == 1) {
        player.freezed = false;
        player.talking = false;
      }
    });
    var doorkeeper_init = false;
    var dialog = 1;
    var reservation_closed_info = false;
    new Element(new Graphic('entity', entities, 'city', 'doorkeeper', 180, 42), 0,function(element){
      element.graphic.hide(decor);
      element.graphic.hide(ground);
      if (!Math.round(time/1000) 
      && !reservation_closed_info) {
        reservation_closed_info = true;
        player.freezed = true;
        player.talking = true;
        sfx("start");
        new Element(new Graphic('dialog', infos, 'city', 'timeout', Math.round(w/2-16), 
        Math.round(h/2-3)), 0, function(element){
          element.graphic.hide(decor);
          element.graphic.hide(ground);
          element.graphic.hide(entities);
          if (keys['enter'] && keys_up['enter']) {
            keys['enter'] = false;
            keys_up['enter'] = false;
            decor.hide = {};
            ground.hide = {};
            entities.hide = {};
            time = 0;
            player.freezed = false;
            player.talking = false;
            erase(element);
          }
        });
      }
      if (reservation_closed_info) {
        erase(element);
        decor.hide = {};
        ground.hide = {};
        entities.hide = {};
      }
      if ( element.graphic.collideWith(player.graphic,-3,0)
        || element.graphic.collideWith(player.graphic,3,0)) {
        if ( doorkeeper_init ) {
          element.graphic.changeSequence('doorkeeper_price');
          if (keys['enter'] && player.coins >= 150 && time && !player.cant_behave) {
            keys['enter'] = false;
            keys_up['enter'] = false;
            decor.hide = {};
            ground.hide = {};
            entities.hide = {};
            erase(player);
            time = 0;
            player.coins = 0;
            shutdown(current_level);
            element.graphic.hide(decor);
            element.graphic.hide(ground);
            element.graphic.hide(entities);
            boot_level(current_level + 1)
          }
        } else if (dialog == 1) {
          element.graphic.changeSequence('doorkeeper_init');
          if (keys['enter']) {
            player.freezed = true;
            player.talking = true;
            keys['enter'] = false;
            keys_up['enter'] = true;
            dialog = 3;
            new Element(new Graphic('dialog', infos, 'city', 'dialog1', Math.round(w/2-17), 
            Math.round(h/2-4)), 0, function(element){
              element.graphic.hide(decor);
              element.graphic.hide(ground);
              element.graphic.hide(entities);
              if (keys['enter'] && keys_up['enter']) {
                keys['enter'] = false;
                keys_up['enter'] = false;
                decor.hide = {};
                ground.hide = {};
                entities.hide = {};
                boot_log(true);
                bg.boot_collectibles();
                sfx("start");
                time = 120000;
                doorkeeper_init = true;
                player.freezed = false;
                player.talking = false;
                dialog = 1;
                erase(element);
              }
            });
          }
        }
      } else {
        element.graphic.changeSequence('doorkeeper');
      }
    });
    
    /* ### init player */
   
    boot_player(32, 48);
  }, 
  function() {
    // @LEVEL4
   set_mood("#000", "#C00", "#FEE", "#600", 'la_roue');

    /* ## init ground elements */

    new Element(new Graphic('ground', ground, 'hotel', 'ground', 0, 0), 0, function(element){
    }, 0, 0, true);
    new Element(new Graphic('decor', decor, 'hotel', 'decor', 0, 0), 0, function(element){
    }, 0, 0, true);
    
    /* ## init decor */

    //new Element(new Graphic('decor', decor, 'title', 'decor', 0, 0), 0);
    //new Element(new Graphic('decor', decor, 'intro', 'decorB', 0, -56), 0);
    //new Element(new Graphic('decor', decor, 'intro', 'decorC', 0, -56*2), 0);
    dialog = 1;
    new Element(new Graphic('magician', entities, 'magician', 'stand', 13, 4), 0, function(element) {
      if (card) {
        if (dialog == 1 && element.graphic.collideWith(player.graphic,3,0)) {
          dialog = 1.5;
          player.freezed = true;
          player.talking = true;
          new Element(new Graphic('dialog', infos, 'hotel', 'dialog1', w/2-17, 0), 0, function(element){
            element.graphic.hide(decor);
            element.graphic.hide(ground);
            element.graphic.hide(entities);
            if (keys['enter']) {
              dialog = 2;
              keys['enter'] = false;
              keys_up['enter'] = false;
              erase(element);
            }
          });
        }
        if (dialog == 2){
          dialog = 2.5;
          new Element(new Graphic('dialog', infos, 'hotel', 'dialog2', w/2-17, 0), 0, function(element){
            element.graphic.hide(decor);
            element.graphic.hide(ground);
            element.graphic.hide(entities);
            if (keys['enter'] && keys_up['enter']) {
              dialog = 3;
              keys['enter'] = false;
              keys_up['enter'] = false;
              erase(element);
            }
          });
        }
        if (dialog == 3){
          dialog = 3.5;
          new Element(new Graphic('dialog', infos, 'hotel', 'dialog3', w/2-17, 0), 0, function(element){
            element.graphic.hide(decor);
            element.graphic.hide(ground);
            element.graphic.hide(entities);
            if (keys['enter'] && keys_up['enter']) {
              dialog = 4;
              keys['enter'] = false;
              keys_up['enter'] = false;
              erase(element);
            }
          });
        }
        if (dialog == 4){
          dialog = 4.5;
          new Element(new Graphic('dialog', infos, 'hotel', 'dialog4', w/2-17, 0), 0, function(element){
            element.graphic.hide(decor);
            element.graphic.hide(ground);
            element.graphic.hide(entities);
            if (keys['enter'] && keys_up['enter']) {
              dialog = 5;
              keys['enter'] = false;
              keys_up['enter'] = false;
              erase(element);
            }
          });
        }
        if (dialog == 5){
          dialog = 5.5;
          new Element(new Graphic('dialog', infos, 'hotel', 'dialog5', w/2-17, 0), 0, function(element){
            element.graphic.hide(decor);
            element.graphic.hide(ground);
            element.graphic.hide(entities);
            if (keys['enter'] && keys_up['enter']) {
              win = true;
              player.freezed = false;
              player.talking = false;
              keys['enter'] = false;
              keys_up['enter'] = false;
              erase(element);
              shutdown();
              boot_level(0);
            }
          });
        }
      } else {
        if (dialog == 1 && element.graphic.collideWith(player.graphic,3,0)) {
          dialog = 1.5;
          player.freezed = true;
          player.talking = true;
          new Element(new Graphic('dialog', infos, 'hotel', 'dialog1B', w/2-17, 0), 0, function(element){
            element.graphic.hide(decor);
            element.graphic.hide(ground);
            element.graphic.hide(entities);
            if (keys['enter']) {
              dialog = 2;
              keys['enter'] = false;
              keys_up['enter'] = false;
              erase(element);
            }
          });
        }
        if (dialog == 2){
          dialog = 2.5;
          new Element(new Graphic('dialog', infos, 'hotel', 'dialog2B', w/2-17, 0), 0, function(element){
            element.graphic.hide(decor);
            element.graphic.hide(ground);
            element.graphic.hide(entities);
            if (keys['enter'] && keys_up['enter']) {
              dialog = 3;
              keys['enter'] = false;
              keys_up['enter'] = false;
              erase(element);
            }
          });
        }
        if (dialog == 3){
          dialog = 3.5;
          new Element(new Graphic('dialog', infos, 'hotel', 'dialog3B', w/2-17, 0), 0, function(element){
            element.graphic.hide(decor);
            element.graphic.hide(ground);
            element.graphic.hide(entities);
            if (keys['enter'] && keys_up['enter']) {
              player.freezed = false;
              player.talking = false;
              keys['enter'] = false;
              keys_up['enter'] = false;
              erase(element);
              shutdown();
              boot_level(0);
            }
          });
        }
      }
    });
    new Element(new Graphic('dog_entity', entities, 'dog', 'stand_left', 20, 184), 0,function(element){
      element.graphic.hide(decor);
      if ( element.graphic.collideWith(player.graphic,-3,0)
        || element.graphic.collideWith(player.graphic,3,0)) {
        if (player.graphic.x > element.graphic.x){
          element.graphic.changeSequence('interact_right');
        } else {
          element.graphic.changeSequence('interact_left');
        }
        if (keys['enter'] && dialog == 1) {
          dialog = 1.5;
          player.freezed = true;
          player.talking = true;
          keys['enter'] = false;
          keys_up['enter'] = true;
          new Element(new Graphic('dialog', infos, 'country', 'dialog2', w-35, 0), 0, function(element){
            element.graphic.hide(decor);
            element.graphic.hide(ground);
            element.graphic.hide(entities);
            if (keys['e']) {
              decor.hide = {};
              ground.hide = {};
              entities.hide = {};
              reboot_level(current_level);
            }
            if (keys['enter'] && keys_up['enter']) {
              keys['enter'] = false;
              keys_up['enter'] = false;
              decor.hide = {};
              ground.hide = {};
              entities.hide = {};
              dialog = 1;
              console.log("coucou",dialog);
              erase(element);
            }
          });
        }
      } else {
        if (player.graphic.x > element.graphic.x){
          element.graphic.changeSequence('stand_right');
        } else {
          element.graphic.changeSequence('stand_left');
        }
      }
      if (player.freezed && dialog == 1) {
        player.freezed = false;
        player.talking = false;
      }
    });

    boot_player(37, 185 /*5*/);
  },
  //function() {
  //  // @LEVEL5
  //  set_mood('#050', '#8C3','#dfd', '#000', 'cascade');
  //  boot_log();
  //  /* ## init ground elements */

  //  new Element(new Graphic('ground', ground, 'dummy_level_1', 'A', 209*0, 55*0), 0);
  //  
  //  /* ### init player */
  // 
  //  boot_player(0, 0);
  //} 
];

// METHODS

const collectibles = [['♠','swords'],['♣','wands'],['♥','cups'],['♦','coins']];
const technicals = [
  ['☻','end']
];

// TODO: DIRTY
var pause = false, verbose = false, startTime = new Date().getMilliseconds(), total_fps = 0, draw_counter = 0, game_speed = 25, fps_refresh = 5, stop = false, current_level, dialog = false, time = Infinity, wanted_framerate = 40, frame_duration = 0, card = false, win = false;

// CHECK visibility

var hidden, visibilityChange;
if (typeof document.hidden !== "undefined") { // Opera 12.10 and Firefox 18 and later support
  hidden = "hidden";
  visibilityChange = "visibilitychange";
} else if (typeof document.msHidden !== "undefined") {
  hidden = "msHidden";
  visibilityChange = "msvisibilitychange";
} else if (typeof document.webkitHidden !== "undefined") {
  hidden = "webkitHidden";
  visibilityChange = "webkitvisibilitychange";
}

document.addEventListener(visibilityChange, function(){
  if (document[hidden]) {
    console.log("bye");
    pause = true;
  } else {
    console.log("hello");
    pause = false;
  }
}, false);

var t0 = false, t1 = false;

function draw() {
  if (stop) return;
  if (pause) return setTimeout(function(){draw()},game_speed);
  t1 = performance.now();
  // <function id='update_framerate?'>
  //let endTime = new Date().getMilliseconds();
  //let current_fps = 1 / ((endTime - startTime)/1000);
  //draw_counter += 1;
  //total_fps += current_fps;
  //let duration = endTime - startTime;
  //if (duration > 0 && Math.round(time/1000)) time -= duration;
  //log( String(Math.round(time/1000)), 'time', true);
  //if (draw_counter == fps_refresh) {
  //  draw_counter = 0;
  //  total_fps /= fps_refresh;
  //  log(Math.round(total_fps)+'FPS ('+game_speed+')', 'framerate', true);
  //  if (total_fps > 35) game_speed++;
  //  if (total_fps < 35) game_speed--;
  //  if (game_speed < 0) game_speed = 0;
  //  total_fps = 0;
  //}
  //startTime = new Date().getMilliseconds();
  if (t0 && t1) {
    let t3 = t1 - t0;
    let fps = 1000 / t3;
    if (t3 > 0 && time > 0) time -= t3;
    log( String(Math.round(time/1000)), 'time', true);
    if (fps > wanted_framerate) {
      game_speed++;
    } else if (fps < wanted_framerate) {
      if (game_speed > 0) game_speed--;
    }
    log(Math.round(fps)+'FPS ('+game_speed+')', 'framerate', true);
  }
  t0 = performance.now();
  if (player) {
    // </function>
    // <function id='update_log'>
    // log('♠ ' + player.swords, 'swords', true); 
    // log('♣ ' + player.wands, 'wands', true); 
    // log('♥ ' + player.cups, 'cups', true); 
    log('♦ ' + player.coins, 'coins', true);
    // </function>
    // <function id='udpate_cam'>
// camera
// events.push(function(){
//     if ( active_layer.elements['player'][0] < active_layer.w/3 
//     ||   active_layer.elements['player'][0] > active_layer.w/3*2 ){
//       for ( var i in active_layer.elements ) active_layer.moveElement(i,[player.dir[0]*-1+player.run*-1,0]);
//       for ( var i in bg_layer.elements ) bg_layer.moveElement(i,[player.dir[0]*-1+player.run*-1,0]);
//       active_layer.clear();
//     };
//     if ( active_layer.elements['player'][1] < active_layer.h/3 
//     ||   active_layer.elements['player'][1] > active_layer.h/3*2 ){
//       for ( var i in active_layer.elements ) active_layer.moveElement(i,[0,player.dir[1]*-1]);
//       for ( var i in bg_layer.elements ) bg_layer.moveElement(i,[0,player.dir[1]*-1]);
//       active_layer.clear();
//     };
// });
    if (player.graphic.x + entities.camX < w/3){
      entities.camX = -player.graphic.x + player.graphic.canvas.w/3;
      ground.camX = -player.graphic.x + player.graphic.canvas.w/3;
      decor.camX = -player.graphic.x + player.graphic.canvas.w/3;
    }
    if (player.graphic.x + entities.camX > w/3 *2){
      entities.camX = -player.graphic.x + player.graphic.canvas.w/3 * 2;
      ground.camX = -player.graphic.x + player.graphic.canvas.w/3 * 2;
      decor.camX = -player.graphic.x + player.graphic.canvas.w/3 * 2;
    }
    if (player.graphic.y + entities.camY < h/3){
      entities.camY = -player.graphic.y + player.graphic.canvas.h/3;
      ground.camY = -player.graphic.y + player.graphic.canvas.h/3;
      decor.camY = -player.graphic.y + player.graphic.canvas.h/3;
    }
    if (player.graphic.y + entities.camY > h/3 *2){
      entities.camY = -player.graphic.y + player.graphic.canvas.h/3 * 2;
      ground.camY = -player.graphic.y + player.graphic.canvas.h/3 * 2;
      decor.camY = -player.graphic.y + player.graphic.canvas.h/3 * 2;
    }
    ////entities.camX = -player.graphic.x + player.graphic.canvas.w/2;
    //entities.camY = -player.graphic.y + player.graphic.canvas.h/1.75;
    ////ground.camX = -player.graphic.x + player.graphic.canvas.w/2;
    //ground.camY = -player.graphic.y + player.graphic.canvas.h/1.75
    ////decor.camX = -player.graphic.x + player.graphic.canvas.w/2;
    //decor.camY = -player.graphic.y + player.graphic.canvas.h/1.75
  }
  // </function>
  // <function id='update_display'>
  entities.update();
  ground.update();
  decor.update();
  for (var i = 0; i < elements.length; i++) {
    if (elements[i].cant_behave ) continue;
    element = elements[i];
    //if (!element.graphic.canvas.inView(element.graphic.name)) continue;
    if (element.behaviour) element.behaviour(element);
  };
  if (document.getElementById('entities').innerText != entities.output) {
    document.getElementById('entities').innerText = entities.output;
  }
  if (document.getElementById('ground').innerText != ground.output) {
    document.getElementById('ground').innerText = ground.output;
  }
  if (document.getElementById('decor').innerText != decor.output) {
    document.getElementById('decor').innerText = decor.output;
  }
  // </function>
  setTimeout(draw,game_speed);
  //if (verbose) console.log('draw in', endTime - startTime, 'instants');
};

// ELEMENTS

elements = [];

// TODO: pass args to JSON described options
Element = function(graphic, speed, behaviour, jumpCapacity, maxRunSpeed, collectible) {
  this.graphic = graphic;
  this.speed = speed;
  this.behaviour = behaviour;
  this.xVelocity = 1;
  this.yVelocity = 1;
  this.jumping = false;
  this.double_jumping = false;
  this.dashing = false;
  this.jumpCapacity = jumpCapacity ? jumpCapacity : 7;
  this.maxRunSpeed = maxRunSpeed ? maxRunSpeed : 3;
  this.jumpTime = this.jumpCapacity;
  this.jumpTimeCounter = 0;
  this.dirX = 0;
  this.dirY = 0;
  this.coins = 0;
  this.cups = 0;
  this.swords = 0;
  this.wands = 0;
  this.cant_behave = false;
  this.freezed = false;
  this.talking = false;
  this.fall_quantity = 0; // weird name
  if (collectible) this.boot_collectibles();
  this.boot_technicals();
  elements.push(this);
}

Element.prototype.boot_collectibles = function(){
  if (!this.graphic.sequences['Collectibles']) return false;
  for (var i = 0; i < collectibles.length; i++ ) {
    var collectibles_pos = this.graphic.getCharPos(collectibles[i][0],'Collectibles', 0);
    for (var j = 0; j < collectibles_pos.length; j++){
      new Element(
        new Graphic(collectibles[i][1], entities, 'collectible', collectibles[i][0], 
          collectibles_pos[j][0]+this.graphic.x, 
          collectibles_pos[j][1]+this.graphic.y), 0,
          function(element) {
            if (element.graphic.collideWith(player.graphic,0,0)) {
              player[this]++;
              sfx("coin");
              erase(element);
            }
      }.bind(collectibles[i][1]));
    };
  }
}
Element.prototype.boot_technicals = function(){
  if (!this.graphic.sequences['Collectibles']) return false;
  for (var i = 0; i < technicals.length; i++ ) {
    var technicals_pos = this.graphic.getCharPos(technicals[i][0],'Collectibles', 0);
    for (var j = 0; j < technicals_pos.length; j++){
      new Element(
        new Graphic(technicals[i][1], entities,

          technicals[i][1] == 'end' ? 'flag' : false, 
  
          'A',

          technicals_pos[j][0]+this.graphic.x, 
          technicals_pos[j][1]+this.graphic.y), 0,
          technicals[i][1] == 'end' ? (
          function(element) {
            if (element.graphic.collideWith(player.graphic,0,0)) {
              if (current_level == 1){
                 level = 2;
              }else if (current_level == 2) level = 0;
                shutdown();
                boot_level(level);
            }
          } ) : false, 
      );
    };
  }
}

// OK I guess
Element.prototype.move = function(dirX, dirY, sequence, speed) {
  if (sequence){
    if (element.dashing) { 
      this.graphic.animate(sequence, element.speed);
    } else {
      // TODO not working ?
      this.graphic.animate(sequence, speed < 0.3 ? speed : speed/6 + 0.25);
    }
  }
  this.graphic.x += dirX;
  this.graphic.y += dirY;
}

// OK I guess
Element.prototype.collide = function(dirX, dirY) {
  if (this.graphic.collide(ground, dirX, dirY)) {
    return true; 
  }
  return false; 
}

// functions

// OK I guess
function erase(element){
  // delete element graphic
  var index = element.graphic.canvas.graphics.indexOf(element.graphic);
  if (index > -1) {
    element.graphic.canvas.graphics.splice(index, 1);
  }
  // delete element
  index = elements.indexOf(element);
  if (index > -1) {
    elements.splice(index, 1);
  }
};

function moving(element, dirX, dirY, sequences, speed
  /*
    [ upleft,         up, upright, 
      left,     standing, right, 
      downleft,     down, downright ]
  */) { // TODO Math.ceil and Math.floor
  var sequence_dirY = dirY;
  var sequence_dirX = dirX;
  if (element.collide(dirX, 0)) {
    dirX = 0;
    //element.dashing = false;
  }
  if (element.collide(0, dirY)) {
    dirY = 0;
  }
  //<function id='find_sequence'>
  let sequence = 
      dirX <  0 
   && dirY <  0 ? sequences[0]:
      dirX == 0 
   && dirY <  0 ? sequences[1]:
      dirX >  0 
   && dirY <  0 ? sequences[2]:
      dirX <  0 
   && dirY == 0 ? sequences[3]:
      dirX == 0 
   && dirY == 0 ? sequences[4]:
      dirX >  0 
   && dirY == 0 ? sequences[5]:
      dirX <  0 
   && dirY >  0 ? sequences[6]:
      dirX == 0 
   && dirY >  0 ? sequences[7]: sequences[8];
  let hard_sequence = 
      sequence_dirX <  0 
   && sequence_dirY <  0 ? sequences[0]:
      sequence_dirX == 0 
   && sequence_dirY <  0 ? sequences[1]:
      sequence_dirX >  0 
   && sequence_dirY <  0 ? sequences[2]:
      sequence_dirX <  0 
   && sequence_dirY == 0 ? sequences[3]:
      sequence_dirX == 0 
   && sequence_dirY == 0 ? sequences[4]:
      sequence_dirX >  0 
   && sequence_dirY == 0 ? sequences[5]:
      sequence_dirX <  0 
   && sequence_dirY >  0 ? sequences[6]:
      sequence_dirX == 0 
   && sequence_dirY >  0 ? sequences[7]: sequences[8];
  //</function>
  /* falling */
  if (dirY >= 1){
    element.fall_quantity += Math.round(dirY);
    log('falling for ' + element.fall_quantity, 'initial', true);
    if ( element.fall_quantity >= 15 ) { 
      hard_sequence = 'fall';
      sequence = 'fall';
    }
  } else {
    if (element.fall_quantity >= 15 && element.collide(0,1)) { 
      log('hurt by falling', 'initial', true);
      element.fall_quantity = 0;
      element.cant_behave = true;
      element.graphic.changeSequence('crush');
      return setTimeout(function(){
        if (this == player && this.cups <= 0) {
          this.graphic.changeSequence('game_over');
          sfx("death");
          return setTimeout(function(){
              reboot_level(current_level);
          }.bind(element), 2000);
        } else {
          this.cant_behave = false;
          element.cups--;
        }
      }.bind(element),(game_speed + 1) * 30);
    } else {
      element.fall_quantity = 0;
      log('not falling', 'initial', true);
    }
  }
  if (element.jumping && !element.dashing) {
    element.graphic.changeSequence(hard_sequence);
  } else if (!element.dashing) {
    element.graphic.changeSequence(sequence);
  }
  if (!dirX && !dirY) {
    //element.dashing = false;
    if (!element.dashing) return;
  }
  if (element.jumping) {
    element.move(dirX, dirY, hard_sequence, speed);
  } else { 
    element.move(dirX, dirY, sequence, speed);
  }
}

// OK I guess
function accelerate(element, axis, factor) {
  if (!axis && !element.jumping) {
    element.xVelocity *= element.xVelocity * factor < element.maxRunSpeed && element.xVelocity * factor > -element.maxRunSpeed ? factor: 1;
  } else {
    element.yVelocity *= factor;
  } 
}

// OK I guess need to be a element prototype maybe 
function control(element, x, y) {
  element.dirY = y;
  if (element.freezed) return;
  element.dirX = x;
}

// Situation to avoid in the game?
function keep_down(element) {
  // if (element.graphic.collide(ground, element.graphic.name, 0, -1)
  // || (element.graphic.canvas.name == 'interactive' ? false : (element.graphic.collide(interactive, element.graphic.name, 0, -1))) ) {
  //   keys['s'] = true;
  // } else {
  //   var keydown = false;
  //   if (common_keys['s']) keydown = true;
  //   if (!keydown) keys['s'] = false;
  // }
}


// Need to look more into it
function jump(element) {
  if (!element.jumping) return;
  if (element.jumpTimeCounter < element.jumpTime/2) {
    element.dirY = -1;
    element.yVelocity /= 1+1/(element.jumpTime/1.5);
  } else  {
    element.dirY = 1;
    element.yVelocity *= 1+1/element.jumpTime;
  }
  element.jumpTimeCounter+=0.5;
  if (keys.arrowup && element.jumpTimeCounter < element.jumpTime/2 
  && !element.collide(-1, 0)
  && !element.collide( 1, 0) ) { 
    element.jumpTime+=0.6;
  }
  let xVelocity = element.xVelocity * element.dirX;
  let yVelocity = element.yVelocity * element.dirY;
  if (element.jumpTimeCounter > element.jumpTime
 || element.jumpTimeCounter > element.jumpTime/2
 && element.collide(0, 2)
 || element.collide(0, -1) 
 && element.jumpTimeCounter > element.jumpTime/2) {
    stop_jump(element);
  }
}

function stop_jump(element) {
  element.jumping = false;
  element.jumpTimeCounter = 0;
  element.yVelocity = 1;
  element.jumpTime = element.jumpCapacity;
  element.doublejump = false;
}

// SHITTY FUNCTION ? OK I function I guess
function obstruct(element) {
  let dirs = [ 
    [-1,-1], [0,-1], [1,-1],
    [-1, 0], [0, 0], [1, 0],
    [-1, 1], [0, 1], [1, 1],
  ];
  dirs.shuffle();
  if (element.collide(0, 0)) {
    element.graphic.x = Math.round(element.graphic.x);
    element.graphic.y = Math.round(element.graphic.y);
    for (var i = 0; i < dirs.length; i++) {
      if (!element.collide(dirs[i][0], dirs[i][1])){
        return element.move(dirs[i][0], dirs[i][1]);
      } 
    }
    element.graphic.animate('crush', element.speed);
  }
}

// LOG (place it somewhere else more organized)

function log(str , where, replace) {
  if (replace) {
    sprites['log'][where][0][0] = str;
  } else {
    sprites['log'][where][0].push(str);
  }
  infos.update();
  document.getElementById('infos').innerText = infos.output;
}

// SETUP

var w, h;
function setup(files, i){ // NOT ACCURATE NAME
  if (!i) {
    i = 0;
    boot_log();
  }
  return readGraphic('sprites/' + files[i]).then(graphic => {
    if (i == files.length-1) {
      sprites[files[i].substring(0, files[i].length)] = graphic;
      log('setup ' + files[i], 'initial'); 
      setup_done = true;
      shutdown();
      return new Date().getTime();
    }
    sprites[files[i].substring(0, files[i].length)] = graphic;
    log('setup ' + files[i], 'initial'); 
    return setup(files, i + 1);
  })
};

var ground, entities, player, setup_done = false;
let setupStartTime = new Date().getTime();

function boot_level(level){
  decor.hide = {};
  ground.hide = {};
  entities.hide = {};
  infos.hide = {};
  time = Infinity;
  levels[level]();
  current_level = level;
  entities.camX = -player.graphic.x + player.graphic.canvas.w/2;
  entities.camY = -player.graphic.y + player.graphic.canvas.h/1.75;
  ground.camX = -player.graphic.x + player.graphic.canvas.w/2;
  ground.camY = -player.graphic.y + player.graphic.canvas.h/1.75
  decor.camX = -player.graphic.x + player.graphic.canvas.w/2;
  decor.camY = -player.graphic.y + player.graphic.canvas.h/1.75
  console.log('level '+level+' boot ok'); 
};

function shutdown(){
  while (elements.length) erase(elements[0]);
  console.log('shutdown ok');
}

function reboot_level(level){
  //stop = true;
  shutdown();
  boot_level(level);
  //stop = false;
  //draw();
}

var start_delay = 0;

function fullscreen() {
  start_delay = 1000;
  document.documentElement.classList.add("no_cursor");
  window.scrollTo(0, 0);
  var elem = document.body;
  if (elem.requestFullscreen) {
    elem.requestFullscreen();
  } else if (elem.webkitRequestFullscreen) { /* Safari */
    elem.webkitRequestFullscreen();
  } else if (elem.msRequestFullscreen) { /* IE11 */
    elem.msRequestFullscreen();
  }
}

document.addEventListener('keyup', function(e){
  if (e.key == 'Enter' && !setup_done) { 
    fullscreen();
    setTimeout(function(){
      if (window.innerWidth >= 1440) {
        w = parseInt(window.innerWidth/18)-1;
        h = parseInt(window.innerHeight/36)-1;
      } else {
        w = Math.floor(window.innerWidth/9)-1;
        h = Math.floor(window.innerHeight/18)-1;
      }
      /* <function id='init_canvas_layers'> */
      entities = new Canvas('entities', w, h);
      ground = new Canvas('ground', w, h);
      decor = new Canvas('decor', w, h);
      infos = new Canvas('infos', w, h);
      /* </function> */
      /* init

      /* init log */
      sprites['collectible'] = {
        '♠' : [['♠']],
        '♣' : [['♣']],
        '♥' : [['♥']],
        '♦' : [['♦']]
      }
      sprites['log'] = {
        'initial': [[]],  
        'framerate': [[]],
        'time': [[]],
        'swords': [[]],
        'wands': [[]],
        'cups': [[]],
        'coins': [[]]
      }
      //log(qwerty ? 'QWERTY mode': 'AZERTY mode', 'initial');
      draw();
      setup([
        /* characters */
        'player',
        'title',
        'intro',
        'country',
        'dog',
        'magician',
        'city',
        'hotel',
        /* grounds */
        // '00_ground',
        // '01_ground',
        // '02_ground',
        // '03_ground',
        // '04_ground',
        // '05_ground',
        // '06_ground',
        // '10_ground',
        // /* decors */
        // '00_decor',
        // '01_decor',
        // 'flag',
        // 'dummy_level_0',
        // 'dummy_level_1',
      ]).then(endTime => {
        
        log('setup is done in ' + (endTime - setupStartTime) + ' instants', 'initial');
        boot_level(0);
      });
    },start_delay);
  }
});

// LOG

// INPUT

let keys = {};
let keys_up = {};
let common_keys = {};
//let qwerty = true;

document.addEventListener('keydown', function(e) {
  //console.log('keydown',e.key);
  if (start_delay){
    e.preventDefault();
    e.stopPropagation();
  }
  keys[e.key.toLowerCase()] = true;
  common_keys[e.key.toLowerCase()] = true;
});

document.addEventListener('keyup', function(e) {
  keys[e.key.toLowerCase()] = false;
  keys_up[e.key.toLowerCase()] = true;
  common_keys[e.key.toLowerCase()] = false;
  //if (e.key == 'm' || e.key == 'M') {
  //  qwerty = qwerty ? false: true;
  //  log(qwerty ? 'QWERTY mode': 'AZERTY mode', 'initial'); 
  //}
});

console.log('joan_jump.js is ready');

