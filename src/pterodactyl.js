//  Joan Jump is a ASCII art plateform game.
//  Copyright (C) 2021 adelfaure
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <https://www.gnu.org/licenses/>.

// Common

String.prototype.replaceAt = function(index, replacement) {
    return this.substr(0, index) + replacement + this.substr(index + replacement.length);
}

function read(file) {
  return fetch(file)
    .then(response => response.text())
    .then(text => {return text;});
}

// Pterodactyl

function readGraphic(file) {
  return read(file).then(content => {
    let sprite = {};
    let sequences = content.split('@');
    for (var i = 1; i < sequences.length; i++){
      let sequence_name = sequences[i].split('\n',1)[0];
      sprite[sequence_name] = [];
      let frames = sequences[i].split('&\n');
      for (var j = 0; j < frames.length; j++){
        let frame = frames[j].split('\n');
        if (!j) frame.shift();
        frame.pop();
        sprite[sequence_name].push(frame);
      }
    }
    return sprite;
  });
}


// SPRITES 

const sprites = {};

// GRAPHICS

const Graphic = function(name, canvas, sequences, sequence, x, y) {
  if (!x) x = 0;
  if (!y) y = 0;
  this.name = name;
  this.canvas = canvas;
  this.canvas.graphics.push(this);
  this.sequences = sprites[sequences];
  this.sequence = sequence;
  this.sequenceIndex = 0;
  this.chars_pos = [];
  this.chars = [];
  this.x = x;
  this.y = y;
  this.setupDisplay();
}

Graphic.prototype.changeSequence = function(sequence) {
  if (this.sequence != sequence) {
    this.sequence = sequence;
    this.sequenceIndex = 0;
  }
}

Graphic.prototype.animate = function(sequence, speed) {
  this.sequenceIndex += speed;
  if (this.sequence != sequence
   || Math.floor(this.sequenceIndex) > this.sequences[this.sequence].length-1) {
    this.sequence = sequence;
    this.sequenceIndex = 0;
  }
}

Graphic.prototype.setupDisplay = function(){
  let frame = this.sequences[this.sequence][Math.floor(this.sequenceIndex)];
  this.chars = [];
  this.chars_pos = [];
  if (frame == undefined) console.log(this);
  for (var line = 0; line < frame.length; line++){
    for (var cha = 0; cha < frame[line].length; cha++) {
      if (frame[line][cha] == '░') continue;
      this.chars_pos.push(
        parseInt(
          cha + this.x + this.canvas.camX
        )+','+parseInt(
          line + this.y + this.canvas.camY
        )
      );
      this.chars.push(frame[line][cha]);
      //console.log(parseInt(cha + this.x), parseInt(line + this.y), frame[line][cha]);
    }
  }
}

// function check( s1, s2)
// {
//     // Create a map to map
//     // characters of 1st string
//     var map = new Map();
//  
//     // traverse the first string
//     // and create a hash map
//     for (var i = 0; i < s1.length; i++)
//     {
//         if(map.has(s1[i].charCodeAt(0)))
//         {
//             map[s1[i].charCodeAt(0)]++;
//         }
//         else
//         {
//             map[s1[i].charCodeAt(0)]=1;
//         }
//          
//     }
//  
//     // traverse the second string
//     // and if there is any
//     // common character than return 1
//     for (var i = 0; i < s2.length; i++)
//         if (map[s2[i].charCodeAt(0)] > 0)
//             return true;
//  
//     // else return 0
//     return false;
// }
Graphic.prototype.collide = function(canvas, dirX, dirY) {
  for (var i = 0; i < canvas.graphics.length; i++) {
    if (canvas.graphics[i].name == this.name) continue;
    if (this.collideWith(canvas.graphics[i], dirX, dirY)) return true;
  }
  return false;
  //console.log(canvas);
}

Graphic.prototype.getCharPos = function(character, sequence, frame) {
  //console.log(this.sequences[sequence][frame],character);
  let pos = [], i=-1;
  for (var y = 0; y < this.sequences[sequence][frame].length; y++){
    let string = this.sequences[sequence][frame][y];
    while((i=string.indexOf(character,i+1)) >= 0)pos.push([i,y]);
  }
  if (pos.length) {
    return pos;
  } else {
    return false;
  };
};

Graphic.prototype.collideWith = function(graphic, dirX, dirY) {
  let map = ' '+graphic.chars_pos.join(' ')+ ' ';
  let chars_pos = this.chars_pos; 
  //chars_pos.shuffle();
  for (var p = 0; p < chars_pos.length; p++) {
    let pos = chars_pos[p].split(',');
    let char_pos = (
      dirX > 0 ? Math.ceil(Number(pos[0]) + dirX):
      Math.floor(Number(pos[0]) + dirX)
      ) + ',' + (
      dirY > 0 ? Math.ceil(Number(pos[1]) + dirY):
      Math.floor(Number(pos[1]) + dirY))
    let index = map.indexOf(char_pos)
    if (map.indexOf(' '+char_pos+' ') > -1) return true;
  }
  return false;
}

Graphic.prototype.hide = function(canvas) {
  canvas.hide[this.name] = ' '+this.chars_pos.join(' ')+' ';
}

// pterodactyl canvas

const Canvas = function(name, w, h) {
  this.name = name;
  this.camX = 0;
  this.camY = 0;
  this.hide = {};
  this.graphics = [];
  this.build(w,h);
}

Canvas.prototype.build = function(w,h) {
  this.matrix = [];
  this.w = w;
  this.h = h;
  for (var y = 0; y < h; y++){
    this.matrix.push([]);
    for (var x = 0; x < w; x++){
      this.matrix[y].push(' ');
    }
  }
}

Canvas.prototype.clean = function() {
  for (var y = 0; y < this.h; y++){
    for (var x = 0; x < this.w; x++){
      this.matrix[y][x] = ' ';
    }
  }
}

Canvas.prototype.update = function() {
  //let startTime = new Date().getTime();
  this.clean();
  for (var i = 0; i < this.graphics.length; i++) {
    this.graphics[i].setupDisplay();
    loop1:
    for ( var p = 0; p < this.graphics[i].chars_pos.length; p++ ){
      let pos = this.graphics[i].chars_pos[p].split(',');
      if (!this.matrix[pos[1]]) continue;
      if (!this.matrix[pos[1]][pos[0]]) continue;
      for (var h in this.hide) {
        if (this.hide[h].indexOf(' '+pos[0]+','+pos[1]+' ') > -1) continue loop1;
      }
      this.matrix[pos[1]][pos[0]] = this.graphics[i].chars[p];
    }
  }
  let output = '';
  for (var y = 0; y < this.matrix.length; y++){
    output += this.matrix[y].toString()+'\n';// .replace(/\u2588/g,' ')
  }
  this.output = output.replace(/,/g,'')
  //let endTime = new Date().getTime();
  //if (verbose) console.log('update canvas in', endTime - startTime, 'instants');
}

// ADD IN VIEW FN 
Canvas.prototype.inView = function(graphic) {
  /*
  let frame = this.graphics[graphic][2][parseInt(this.graphics[graphic][3])];
  for (var y = 0; y < frame.length; y++){
    let line = parseInt(this.graphics[graphic][1] + y + this.cam[1])
    if (this.matrix[line] != undefined) {
      return true;
    } else {
      continue;
    }
    for (var x = 0; x < frame[y].length; x++) {
      let cha = parseInt(this.graphics[graphic][0] + x + this.cam[0]);
      if (this.matrix[line][cha] != undefined ) {
        return true;
      } else {
        continue;
      }
    }
  }
  return false;
  */
}

console.log('pterodactyl.js is ready');

  /*
Canvas.prototype.collide = function(canvas, graphic, dirX, dirY) {
  let frame = this.graphics[graphic][2][parseInt(this.graphics[graphic][3])];
  let frameX = this.graphics[graphic][0];
  let frameY = this.graphics[graphic][1];
  let frame_coor = [];
  let coor_collide_with = [];
  for (var y = 0; y < frame.length; y++){
    for (var x = 0; x < frame[y].length; x++) {
      if (frame[y][x] == '░') continue;
      frame_coor.push(
        (dirX > 0 ? Math.ceil(frameX + x + dirX): Math.floor(frameX + x + dirX))
        +':'+
        (dirY > 0 ? Math.ceil(frameY + y + dirY): Math.floor(frameY + y + dirY))
      );
    }
  }
  for (var i in canvas.graphics) {
    let frame_collide_with = canvas.graphics[i][2][parseInt(canvas.graphics[i][3])];
    if (frame_collide_with == frame) continue;
    let frameX = canvas.graphics[i][0];
    let frameY = canvas.graphics[i][1];
    for (var y = 0; y < frame_collide_with.length; y++){
      for (var x = 0; x < frame_collide_with[y].length; x++) {
        if (frame_collide_with[y][x] == '░') continue;
        coor_collide_with.push(Math.round(frameX + x)+':'+Math.round(frameY + y));
      }
    }
  }
  for (var i = 0; i < frame_coor.length; i++){
    if (coor_collide_with.indexOf(frame_coor[i]) > -1) {
      this.graphics[graphic][0] = Math.round(this.graphics[graphic][0]);
      this.graphics[graphic][1] = Math.round(this.graphics[graphic][1]);
      return true;
    }
  }
  return false;
}
  */

// 

/*
// main

const Pterodactyl = function(w,h){
  this.output = '';
  this.graphics = {};
  this.w = w;
  this.h = h;
  this.floodfillToKill = {}; 
  this.matrix = [];
  this.matrix_memory = [];
  this.cam = [0,0];
  for ( var y = 0 ; y < this.h ; y ++ ) {
    this.matrix_memory.push([]);
    this.matrix.push([]);
    for ( var x = 0 ; x < this.w ; x ++ ) {
      this.matrix[y].push(' ');
      this.matrix_memory[y].push([' ']);
    };
    this.matrix[y].push('\n');
    this.matrix_memory[y].push(['\n']);
  };
};

Pterodactyl.prototype.clear = function(){
  for ( var i in this.floodfillToKill ) this.floodfillToKill[i] = true;
  for ( var i in this.graphics ){
    this.cleanGraphic(i);
  }
  this.update();
}

Pterodactyl.prototype.buildGraphic = function(name,x,y,cha){
  this.graphics[name] = [x,y,cha,true,[]];
  this.update();
}

Pterodactyl.prototype.removeGraphic = function(name){
  if ( !this.graphics[name] ) return
  this.cleanGraphic(name);
  delete this.graphics[name];
  this.update();
}

Pterodactyl.prototype.writeToGraphic = function(name,str){
  if ( !this.graphics[name] ) return
  this.cleanGraphic(name);
  this.graphics[name][2].push(str);
  this.update();
}

Pterodactyl.prototype.editGraphic = function(name,line,str){
  if ( !this.graphics[name] ) return
  this.cleanGraphic(name);
  this.graphics[name][2][line] = str;
  this.update();
}

Pterodactyl.prototype.rewriteGraphic = function(name,cha){
  if ( !this.graphics[name] ) return
  this.cleanGraphic(name);
  this.graphics[name][2] = cha;
  this.update();
}

Pterodactyl.prototype.moveGraphic = function(name, dir){
  if ( !this.graphics[name] ) return
  this.cleanGraphic(name);
  this.graphics[name][0] += dir[0];
  this.graphics[name][1] += dir[1];
  this.update();
};

Pterodactyl.prototype.placeGraphic = function(name, pos){
  if ( !this.graphics[name] ) return
  this.cleanGraphic(name);
  this.graphics[name][0] = pos[0];
  this.graphics[name][1] = pos[1];
  this.update();
};

Pterodactyl.prototype.cleanGraphic = function (name){
  if ( !this.graphics[name] ) return
  let el = this.graphics[name];
  let x = parseInt(el[0]);
  let y = parseInt(el[1]);
  for ( var a = 0 ; a < el[2].length ; a ++ ) {
    if ( a + y < 0 || a + y > this.h-1 ) continue
    for ( var b = 0 ; b < el[2][a].length ; b ++ ) {
      if ( b + x < 0 || b + x > this.w-1 ) continue
      if ( el[2][a].charAt(b) == '▒' ) continue
      this.matrix_memory[a+y][b+x].pop();
      this.matrix[a+y][b+x] = this.matrix_memory[a+y][b+x][this.matrix_memory[a+y][b+x].length-1];
    };
  };
  this.graphics[name][this.graphics[name].length-1] = true;
}

Pterodactyl.prototype.collideGraphic = function(name, dir){
  if ( !this.graphics[name] ) return false
  this.moveGraphic(name, dir);
  let el = this.graphics[name];
  let x = parseInt(el[0]);
  let y = parseInt(el[1]);
  let collision = [];
  for ( var a = 0 ; a < el[2].length ; a ++ ) {
    if ( a + y < 0 || a + y > this.h-1 ) continue
    for ( var b = 0 ; b < el[2][a].length ; b ++ ) {
      if ( b + x < 0 || b + x > this.w-1            
        || el[2][a].charAt(b) == ' ' ) continue
      let memory_pos = this.matrix_memory[a+y][b+x];
      if ( memory_pos[memory_pos.length-2] != ' ' ) {
        collision.push([[a,b],memory_pos[memory_pos.length-2]]);
      };
    };
  };
  this.moveGraphic(name, [-dir[0],-dir[1]]);
  if ( collision.length ){
    return collision;
  } else {
    return false;
  };
};

Pterodactyl.prototype.collideGraphicTo = function(name, dir, to, exeptions){
  if ( !this.graphics[name] ) return false
  this.moveGraphic(name, dir);
  let el = this.graphics[name];
  let x = parseInt(el[0]);
  let y = parseInt(el[1]);
  let collision = [];
  for ( var a = 0 ; a < el[2].length ; a ++ ) {
    if ( a + y < 0 || a + y > to.h-1 ) continue
    for ( var b = 0 ; b < el[2][a].length ; b ++ ) {
      if ( b + x < 0 || b + x > to.w-1            
        || el[2][a].charAt(b) == ' '
        || el[2][a].charAt(b) == '▒' ) continue
      let memory_pos = to.matrix_memory[a+y][b+x];
      if ( memory_pos[memory_pos.length-1] != ' '
      &&   exeptions.indexOf(memory_pos[memory_pos.length-1]) < 0 ) {
        collision.push([[a,b],memory_pos[memory_pos.length-1]]);
      };
    };
  };
  this.moveGraphic(name, [-dir[0],-dir[1]]);
  if ( collision.length ){
    return collision;
  } else {
    return false;
  };
};

Pterodactyl.prototype.writeGraphic = function (name){
  if ( !this.graphics[name] ) return false
  if ( !this.graphics[name][this.graphics[name].length-1] ) return false;
  let el = this.graphics[name];
  let x = parseInt(el[0]);
  let y = parseInt(el[1]);
  for ( var a = 0 ; a < el[2].length ; a ++ ) {
    if ( a + y < 0 || a + y > this.h-1 ) continue
    for ( var b = 0 ; b < el[2][a].length ; b ++ ) {
      if ( b + x < 0 || b + x > this.w-1 ) continue
      if ( el[2][a].charAt(b) == '▒' ) continue
      let memory_pos = this.matrix_memory[a+y][b+x];
      memory_pos.push(el[2][a].charAt(b));
      this.matrix[a+y][b+x] = memory_pos[memory_pos.length-1];
    };
  };
  this.graphics[name][this.graphics[name].length-1] = false;
};

const queue = [];

Pterodactyl.prototype.floodfill = function(x,y,wat,wit,speed,dirs,lifespan,temp,id,visited){
  if ( !id ) { 
    id = Math.random();
    this.floodfillToKill[id] = false;
  };
  if ( this.floodfillToKill[id] ) return
  if ( !lifespan ) return 
  if ( !visited ) visited = {}
  let x_val = x;
  let y_val = y;
  x = parseInt(x);
  y = parseInt(y);
  if ( y < 0 || y > this.h-1 ) return 
  if ( x < 0 || x > this.w-1 ) return
  if ( this.matrix[y][x] != wat && wat ) return
  if ( this.matrix[y][x] == wit && wat ) return
  if ( visited[y] && visited[y][x]  ) return
  if ( !visited[y] ) visited[y] = {}
  if ( !visited[y][x] ) visited[y][x] = true
  this.matrix_memory[y][x].push(wit);
  this.matrix[y][x] = wit;
  if ( temp ) {
    setTimeout(function(){
      this[0].matrix_memory[this[2]][this[1]].push(' ');
      this[0].matrix[this[2]][this[1]] = ' ';
      this[0].update();
    }.bind([this,x,y]),temp);
  };
  if ( !speed ) {
    for ( var i = 0 ; i < dirs.length ; i++ ){
      this.floodfill(x_val+dirs[i][0],y_val+dirs[i][1],wat,wit,speed,dirs,lifespan-1,temp,id,visited);
    }
  } else {
    setTimeout(function(){
      for ( var i = 0 ; i < dirs.length ; i++ ){
        this.floodfill(x_val+dirs[i][0],y_val+dirs[i][1],wat,wit,speed,dirs,lifespan-1,temp,id,visited);
      }
    }.bind(this),speed);
  }
  this.update();
}

Pterodactyl.prototype.update = function(){
  for ( var i in this.graphics ){
    this.writeGraphic(i);
  };
  let output = this.matrix.toString().replace(/\u2588/g,' ')
  this.output = output.replace(/,/g,'')
}
*/
