- [v0.20.3]
  + Make magician invisible if tuto not done.
  + Prevent Text highlight.
  + Fix the failing in timed events.
  + Add message and sfx for failing timed event.
  + Fix display issue when width is 1440px.
- [v0.20]
  + Add the card item to found.
  + Implement level 4 (ending) ground, decor and dialogs.
  + Make a different ending if item found.
  + If item found you can replay the game in a different style.
  + Add level 3 music : fleur.
  + Change pterodactyl hiding behaviour.
- [v0.19.1]
  + Draw the card item to found.
  + Change framerate handling function and improve global framerate stability and performance.
- [v0.19]
  + Rework global color schemes.
  + Add a sound effect triggering function.
  + Add sound effects : jumping, dogerolling, death, getting coins, starting timed event.
  + Loop music.
  + Add a check visibility function to know if joan_jump is focused by player and pause game if not.
  + Change camera system to follow player only when he is near screen limits.
  + Better "canvas cleaning" after element erasing.
  + Implement level 3 (city) ground, decor and dialogs.
  + Fix ponctuation issues.
- [v0.18]
  + Add a "talking", "freezed" and "can't behave" var for element.
  + Change acceleration speed.
  + Limit jump to double jump but disable swords consumption.
  + Disable wand consumption for antigravitionnal dogerolling but limit it to one per not touching ground period.
  + Rework level 1 (intro) with an interactive tutorial.
  + Rework level 2 with only coins to get, timed bus taking event for 100 coins in 3 minutes, interact with dog as a way to restart level.
  + Work on fullscreen function.
- [0.17]
  + Mostly level flawed design.
  + Work on country level sprite.
  + Work on level 2.
- [0.16]
  + Made title level.
  + Add automne music.
  + Small modif on intro level and sprites.
  + Fix fullscreen issue for chromium based browsers.
  + Add delayed auto fullscreen function.
  + Change min-width screen size for x2 font size to 1440px.
  + Change keys to be arrows based.
  + Change 'enter' to 'space' for the antigravity dogerolling.
- [0.15]
  + Made city level decor.
  + Start working on country level decor.
  + Add la_roue music.
  + Make introduction level (ground, decor and implementation).
  + Implement the first dialogs.
  + Add a freezed status for Elements.
  + Add a silent statut for boot_log.
- [0.14]
  + Made a 22 levels color palette.
  + Made a JSON containg levels names and hex values.
  + Simplify the collide function in pterodactyl (by the use of collideWith).
- [0.13]
  + Make generic boot_level function to boot any level.
  + Make a boot player function.
  + Make a boot log function.
  + Make element boot_collectible function.
  + Fix speed feeling by reduce animation speed when upper that 0.3 frame per behaviour update.
  + Make dummy_level_0.
  + Make dummy_level_1.
  + Prepare technicals (end entity for now).
  + Make a flag sprite.
  + Fix moving flaw that allow to not die when constantly press jumping key when falling.
  + Add a set_mood function to change colors and music for each level.
  + Start to work on color palettes.
- [0.12]
  + Implementing death by falling.
  + Make boot, shutdown and reboot function (use in death by falling).
  + Add a warning note for arch user using i3 with the 89+ version of firefox.
  + Fall sequence used when player will by hurt by falling (20+ chars fall).
  + Add all 4 collectible in the game to test added lifes.
  + Make a stop jump function.
  + Swords can now be use to make double jump.
  + Add damage animation (crush).
  + Add game over animation.
- [0.11]
  + Fix dashing flaw that consume multiple wands when active the command.
  + Try a dirty way to choose fps, currently 40 seems good to feeling.
  + Add swords and cups counter.
  + Wands can be collected.
- [0.10]
  + Add some screenshots to itch page.
  + Declare player entity after the others.
  + Draw collectible and include it in jgs font.
  + Remove "interactive" named layer (entities and ground are allready interactive).
  + Comment the "keep down" function for the time being.
  + Start working on including collectible in the game with a @collectible sequence in ground sprites)
  + Add a getCharPos function for graphics in previous to get collectible emplacement from @collectible sequence and place it on the game as entities elements.
  + Add some sort of dash consuming wands thats will be a collectible.
  + Thanks to @DarioDaf and @Rosomak360 from the SoloDevelopment discord I can call the "dash" "Antigravity dogerolling".
- [0.9]
  + Include jgs font specimen link.
  + Continue working on level 0.
  + Start working on level 1.
  + Change instants counter to FPS counter.
  + Change jump behaviour still some flaw but I am finally proud of it.
  + Draw and include magician sprite.
  + Find ffmpeg ogg compression to reduce mp3 files to 1-1.5mo
- [0.8]
  + Make draft ground and decor for level 0.
  + Change transparency char from █ to ▒.
  + Change "jump curve/behaviour" a bit.
  + Remove a jump flaw that make player climb very high when jumping against a wall.
- [0.7]
  + Element can't accelerate while jumping.
  + Change jumping time modifications conditions.
  + Make a empty level "chunk" template (ground), based on 1920x1080 resolution/vim lines, 9x18 font ratio w209 h56.
  + Remove mp3 file from all revisions feed.
  + Draw dog sprite.
- [0.6]
  + Start to drawing the first level.
  + Change layers/canvas names to 'entitites', 'interactive', 'ground', 'decor' and 'infos' (in order of superposition).
  + Add a hide function to be able to cancel glyphs superposition (between player element and decor canvas for exemple).
  + Begin to implement music in the game.
  + Start working on color schemes.
  + Change jump inertia to be smoother.
- [0.5]
  + Crush animation and triggering when obstructed.
  + Collide top when jumping stop the jump.
  + Display drawing instants top left.
  + canvas responsive.
- [0.4] 
  + Dirty fix a "levitation issue" in certains case pressing space did that the
    jump will never totally end making the player levitate one case above ground
  + Rename Sprite to Graphic (because sprites are not "pterodactyl graphics").
  + Add a jump capacity variable.
- [0.3] collision and display reworked 
- [0.2] 
  + running stop when collided
  + decellerate when no directions keys down 
  + making collisions work behind view by checking coordinates and not glyphs
  + correct math.ceil or floor when collision
  + stop update canvas text output when no modifications
  + integer animated assets loading
- [0.1] pre-solodevjam prototype
  + [0.0.6] animated levels
  + [0.0.5] layer system
  + [0.0.4] level loading
  + [0.0.3] level drawing
  + [0.0.2] effects, colors, and better collisions
  + [0.0.1] move, jumb, collision
